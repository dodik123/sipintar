<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
<div class="">
    <div class="table-toolbar">
     <div class="btn-group">
      <!--<a href="<?php echo base_url() . $module . '/add' ?>"><button class="btn btn-success">Tambah <i class="icon-plus icon-white"></i></button></a>-->
     </div>
     <div class="btn-group pull-right">
      <!--<button data-toggle="dropdown" class="btn dropdown-toggle">Tools <span class="caret"></span></button>-->
<!--      <ul class="dropdown-menu">
       <li><a href="#">Save as PDF</a></li>
       <li><a href="#">Export to Excel</a></li>
      </ul>-->
     </div>
    </div>
    <br/>

    <br/>
    <br/>

    <div class="message">

    </div>
    <div class="data">
     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_pengaturan_nilai">
       <thead>
        <tr class="sticky-row">
         <th>No</th>
         <th>Status</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_pengaturan_nilai)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_pengaturan_nilai as $value) { ?>
          <tr class="odd gradeX">
           <td><?php echo $no++ ?></td>
           <td id="nilai_status">Nilai <?php echo $value['show'] == true ? 'Ditampilkan' : 'Tidak Ditampilkan' ?></td>
           <td class="center">
            <button id="" 
                    onclick="pengaturan_nilai_data.tampilkan(this, '<?php echo $value['id'] ?>')" class="btn btn-warning">
             Tampilkan
            </button>
            
            <button id="" 
                    onclick="pengaturan_nilai_data.tidakTampilkan(this, '<?php echo $value['id'] ?>')"
                    class="btn btn-danger">
             Jangan Tampilkan
            </button>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>     
    </div>        
   </div>
  </div>
 </div>
 <!-- block -->
<!-- <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Data <?php echo $title ?></div>
   <div class="pull-right"><span class="badge badge-info"><?php echo count($data_pengaturan_nilai) ?></span>

   </div>
  </div>
  <div class="block-content collapse in">
   
  </div>
 </div>-->
 <!-- /block -->
</div>