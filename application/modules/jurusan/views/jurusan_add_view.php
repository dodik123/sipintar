<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
<div class="">
    <form class="form-horizontal">
     <div class="message">

     </div>
     <fieldset>
      <legend>Tambah <?php echo $title ?></legend>
      <input type="hidden" id="id" class="" value="<?php echo isset($id) ? $id : '' ?>"/>
      <div class="control-group">
       <label class="control-label" for="focusedInput">Jurusan</label>
       <div class="controls">
        <input class="input-xlarge focused required" id="jurusan" type="text" value="<?php echo isset($jurusan) ? $jurusan : '' ?>" 
               placeholder="Nama Jurusan" error="Jurusan">
       </div>
      </div>
      <div class="">
       <button type="button" class="btn btn-primary" onclick="jurusan_data.simpan()">Simpan</button>
       <a href="<?php echo base_url() . $module ?>"><button type="button" class="btn btn-success">Kembali</button></a>
      </div>
     </fieldset>
    </form>

   </div>
  </div>
 </div>
 <!-- block -->
<!-- <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Form Tambah <?php echo $title ?></div>
  </div>
  <div class="block-content collapse in">
   
  </div>
 </div>-->
 <!-- /block -->
</div>