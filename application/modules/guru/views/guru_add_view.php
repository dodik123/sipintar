<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
   <div class="">
    <form class="form-horizontal">
     <fieldset>
      <legend>Tambah <?php echo $title ?></legend>
      <div class="message">

      </div>
      <input type="hidden" id="id" class="" value="<?php echo isset($id) ? $id : '' ?>"/>
      <div class="control-group">
       <label class="control-label" for="focusedInput">Nama</label>
       <div class="controls">
        <input class="input-xlarge focused required" id="nama" type="text" 
               value="<?php echo isset($nama) ? $nama : '' ?>" 
               error = "Nama" placeholder="Nama Guru">
       </div>
      </div>
      <div class="control-group">
       <label class="control-label">Nip</label>
       <div class="controls">
        <input class="input-xlarge focused required" id="nip" type="text" 
               value="<?php echo isset($nip) ? $nip : '' ?>" 
               error = "Nip" placeholder="Nip Guru">
       </div>
      </div>
      <div class="control-group">
       <label class="control-label">Password</label>
       <div class="controls">
        <input class="input-xlarge focused required" id="password" type="text" 
               value="<?php echo isset($password) ? $password : '' ?>" 
               error="Password" placeholder="Password Guru">
       </div>
      </div>
      <div class="control-group">
       <label class="control-label" for="disabledInput">Mata Pelajaran</label>
       <div class="controls">
        <select class="span6 m-wrap required" name="mata_pelajaran" 
                id="mata_pelajaran" error="Mata Pelajaran">
         <option value="">Pilih Mata Pelajaran</option>
         <?php foreach ($list_mapel as $value) { ?>
          <option value="<?php echo $value['id'] ?>" 
                  <?php echo isset($mata_pelajaran) ? $mata_pelajaran == $value['id'] ? 'selected' : '' : '' ?>>
                   <?php echo $value['mata_pelajaran'] ?>
          </option>
         <?php } ?>
        </select>
       </div>
      </div>
      <div class="">
       <button type="button" class="btn btn-primary" onclick="guru_data.simpan()">Simpan</button>
       <a href="<?php echo base_url() . $module ?>"><button type="button" class="btn btn-success">Kembali</button></a>
      </div>
     </fieldset>
    </form>

   </div>
  </div>
 </div>
 <!-- block -->
 <!-- <div class="block">
   <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">Form Tambah <?php echo $title ?></div>
   </div>
   <div class="block-content collapse in">
    
   </div>
  </div>-->
 <!-- /block -->
</div>