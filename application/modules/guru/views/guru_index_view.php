<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="<?php echo base_url().$module ?>" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
   <div class="">
    <div class="table-toolbar">
     <div class="btn-group">
      <a href="<?php echo base_url() . $module . '/add' ?>"><button class="btn btn-success">Tambah <i class="icon-plus icon-white"></i></button></a>
     </div>
<!--     <div class="btn-group pull-right">
      <button data-toggle="dropdown" class="btn dropdown-toggle">Tools <span class="caret"></span></button>
      <ul class="dropdown-menu">
       <li><a href="#">Save as PDF</a></li>
       <li><a href="#">Export to Excel</a></li>
      </ul>
     </div>-->
    </div>
    <br/>

    <div class="table-toolbar">
     <div class="btn-group pull-right">
      <input class="input-xlarge focused" id="search" type="text" value="" 
             placeholder="Pencarian" onkeyup="guru_data.search(this, event)">
     </div>
    </div>

    <br/>
    <br/>

    <div class="message">

    </div>
    <div class="data">
     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_guru">
       <thead>
        <tr class="sticky-row">
         <th>No</th>
         <th>Nama</th>
         <th>Nip</th>
         <th>Mata Pelajaran</th>
         <th>Password</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_guru)) { ?>
         <?php $no = $this->uri->segment(3) +1; ?>
         <?php foreach ($data_guru as $value) { ?>
          <tr class="odd gradeX">
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['nama'] ?></td>
           <td><?php echo $value['nip'] ?></td>
           <td><?php echo $value['mata_pelajaran'] ?></td>
           <td class="center"><?php echo $value['password'] ?></td>
           <td class="center">
            <a href="<?php echo base_url() . $module . '/edit/' . $value['id'] ?>">
             <i class="icon-edit"></i>
            </a>
            <i class="icon-trash" onclick="guru_data.remove('<?php echo $value['id'] ?>')"></i>           
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>    
     
     <div class="paging text-right">
      <?php echo $pagination ?>
     </div>
    </div>        
   </div>
  </div>
 </div>
 <!-- block -->
 <!-- <div class="block">
   <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">Data <?php echo $title ?></div>
    <div class="pull-right"><span class="badge badge-info"><?php echo count($data_guru) ?></span>
 
    </div>
   </div>
   <div class="block-content collapse in">
    
   </div>
  </div>-->
 <!-- /block -->
</div>