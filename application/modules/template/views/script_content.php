<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/popper.js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/bootstrap/js/bootstrap.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/jquery-slimscroll/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/modernizr/modernizr.js"></script>
<!-- am chart -->
<script src="<?php echo base_url() ?>assets/themes_primary/pages/widget/amchart/amcharts.min.js"></script>
<script src="<?php echo base_url() ?>assets/themes_primary/pages/widget/amchart/serial.min.js"></script>
<!-- Chart js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/chart.js/Chart.js"></script>
<!-- Todo js -->
<script type="text/javascript " src="<?php echo base_url() ?>assets/themes_primary/pages/todo/todo.js "></script>
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/pages/dashboard/custom-dashboard.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/themes_primary/js/script.js"></script>
<script type="text/javascript " src="<?php echo base_url() ?>assets/themes_primary/js/SmoothScroll.js"></script>
<script src="<?php echo base_url() ?>assets/themes_primary/js/pcoded.min.js"></script>
<script src="<?php echo base_url() ?>assets/themes_primary/js/vartical-demo.js"></script>
<script src="<?php echo base_url() ?>assets/themes_primary/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pagination.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootbox.js"></script>
<script src="<?php echo base_url() ?>assets/js/helper.js"></script>
<script src="<?php echo base_url() ?>assets/js/url.js"></script>
<script src="<?php echo base_url() ?>assets/js/message.js"></script>