<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
<div class="">
    <div class="table-toolbar">
     <div class="btn-group">
      <!--<a href="<?php echo base_url() . $module . '/add' ?>"><button class="btn btn-success">Tambah <i class="icon-plus icon-white"></i></button></a>-->
     </div>
<!--     <div class="btn-group pull-right">
      <button data-toggle="dropdown" class="btn dropdown-toggle">Tools <span class="caret"></span></button>
      <ul class="dropdown-menu">
       <li><a href="#">Save as PDF</a></li>
       <li><a href="#">Export to Excel</a></li>
      </ul>
     </div>-->
    </div>
    <br/>

    <div class="table-toolbar">
     <div class="btn-group pull-right">
      <input class="input-xlarge focused" id="search" type="text" value="" placeholder="Pencarian"
             onkeyup="ujian_selesai_dilaksanakan_data.search(this, event)">
     </div>
    </div>

    <br/>    
    <br/>    
    <div class="message">

    </div>
    <div class="data">   
     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_pelajaran">
       <thead>
        <tr class="sticky-row">
         <th class="sticky-cell">No</th>
         <th class="sticky-cell">Kode Ujian</th>
         <th>Token Ujian</th>
         <th>Kategori Soal</th>
         <th>Total Soal</th>
         <th>Nama Ujian</th>
         <th>Pembuat</th>
         <th>Pengawas</th>
         <th>Peserta Ujian</th>
         <th>Limit Waktu Ujian</th>
         <th>Waktu Pelaksanaan</th>
         <th>Tanggal Pelaksanaan</th>
         <th>Tanggal Pembuatan</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_ujian_selesai)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_ujian_selesai as $value) { ?>
          <tr class="odd gradeX">
           <td class="sticky-cell"><?php echo $no++ ?></td>
           <td class="sticky-cell" id="kode_ujian">
            <a href="<?php echo base_url() . $module . '/detailUjian/' . $value['id'] ?>"><?php echo $value['kode_ujian'] ?></a>            
           </td>
           <td><?php echo $value['token'] ?></td>
           <td><?php echo $value['kategori_soal'] ?></td>
           <td><?php echo $value['total_soal'] . ' Butir' ?></td>
           <td><?php echo $value['nama_ujian'] ?></td>
           <td><?php echo $value['guru'] ?></td>
           <td><?php echo $value['pengawas_ujian'] ?></td>
           <td id="peserta_ujian"><?php echo $value['peserta_ujian'] ?></td>
           <td id="limit_waktu_ujian">
            <?php
            if (empty($value['limit_waktu_ujian'])) {
             echo '0';
            } else {
             foreach ($value['limit_waktu_ujian'] as $v_time_limit) {
              echo $v_time_limit['time_limit'] . ' (Menit) ';
             }
            }
            ?>
           </td>
           <td><?php echo $value['waktu_ujian'] ?></td>
           <td><?php echo date('d M Y', strtotime($value['tanggal_ujian'])) ?></td>
           <td><?php echo date('d M Y H:i:s', strtotime($value['tanggal_dibuat'])) ?></td>
           <td>            
            <i class="icon-trash" onclick="ujian_selesai_dilaksanakan_data.remove('<?php echo $value['id'] ?>')"></i>
            <button class="btn btn-warning" id="" 
                    data-original-title="Nilai Seluruh Peserta"
                    onmouseover="message.showCustomTooltip(this, 'left')"                    
                    onclick="nilai_data.detailAllNilai('<?php echo $value['id'] ?>')">
             <i class="icon-th-list icon-white"></i>
            </button>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="14">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>
    </div>        
   </div>
  </div>
 </div>
 <!-- block -->
<!-- <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Data <?php echo $title ?></div>
   <div class="pull-right"><span class="badge badge-info"><?php echo count($data_ujian_selesai) ?></span>

   </div>
  </div>
  <div class="block-content collapse in">
   
  </div>
 </div>-->
 <!-- /block -->
</div>