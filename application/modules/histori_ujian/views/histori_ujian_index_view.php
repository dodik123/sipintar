<div class="row-fluid">
 <div class="card title-module">
  <div class="card-content">
   <i class="mdi mdi-arrow-left mdi-18px hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
   <i class="mdi mdi-arrow-right mdi-18px show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
   <a href="#" class="title-content"><?php echo $title ?></a>
   <hr/>
  </div>
 </div>
</div>

<div class="row-fluid">
 <div class="card">
  <div class="card-content">
<div class="">
    <div class="table-toolbar">
    </div>
    <br/>

    <div class="table-toolbar">
     <div class="btn-group pull-right">
      <input class="input-xlarge focused" id="search" type="text" value="" 
             placeholder="Pencarian" onkeyup="histori_ujian_data.search(this, event)">
     </div>
    </div>

    <br/>
    <br/>

    <div class="message">

    </div>
    <div class="data">
     <div class="sticky-table sticky-headers sticky-ltr-cells">
      <table cellpadding="0" cellspacing="0" border="0" class="" id="tabel_histori_ujian">
       <thead>
        <tr class="sticky-row">
         <th>No</th>
         <th>Nama</th>
         <th>Nis</th>
         <th>Jurusan</th>
         <th>Mata Pelajaran</th>
         <th>Kode Ujian</th>
         <th>Nama Ujian</th>
         <th>Tanggal Ujian</th>
         <th>Waktu Ujian</th>
         <th>Guru Pengajar</th>
         <th>Nilai</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_histori_ujian)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_histori_ujian as $value) { ?>
          <tr class="odd gradeX">
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['siswa'] ?></td>
           <td><?php echo $value['nis'] ?></td>
           <td><?php echo $value['jurusan'] ?></td>
           <td><?php echo $value['mata_pelajaran'] ?></td>
           <td id="kode_ujian">
            <a href="#" onclick="histori_ujian_data.detailUjian('<?php echo $value['ujian_id'] ?>')"><?php echo $value['kode_ujian'] ?></a>            
           </td>
           <td class="center"><?php echo $value['nama_ujian'] ?></td>           
           <td class="center"><?php echo date('d M Y', strtotime($value['tanggal_ujian'])) ?></td>
           <td class="center"><?php echo $value['waktu_ujian'] ?></td>
           <td class="center"><?php echo $value['guru'] ?></td>
           <td class="center"><?php echo $nilai_active == true ? number_format($value['nilai'], 2, ',', '.') : 'Tidak Ditampilkan' ?></td>
           <td>
            <?php if ($nilai_active) { ?>
             <button class="btn btn-warning" id="" 
                     data-original-title="Nilai Seluruh Peserta"
                     onmouseover="message.showCustomTooltip(this, 'left')"                    
                     onclick="nilai_data.detailAllNilai('<?php echo $value['ujian_id'] ?>')">
              <i class="icon-th-list icon-white"></i>             
             </button>
            <?php } else { ?>
             Daftar Nilai Tidak Diaktifkan
            <?php } ?>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="12">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>
    </div>        
   </div>
  </div>
 </div>
 <!-- block -->
<!-- <div class="block">
  <div class="navbar navbar-inner block-header">
   <div class="muted pull-left">Data <?php echo $title ?></div>
  </div>
  <div class="block-content collapse in">
   
  </div>
 </div>-->
 <!-- /block -->
</div>